const portHttp = 80;
const portHttps = 443;

const express = require('express');
const http = require('http');
const https = require('https');
const fs = require('fs');
const cors = require('cors');
const cluster = require('cluster');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const helmet = require('helmet');

const router = express.Router();

module.exports = router;

const expressApp = express();
expressApp.listData = [];
const loggingController = require('./node/logging_controller')(express.Router(), express.Router(), express.Router())
const urlGetter = require('./node/url_getter')(express.Router(), loggingController.save_user_log_db);
const appsflyerPushApi = require('./node/appsflyer_push_api')(express.Router(), express.Router(), urlGetter.get_offers_response);
const data_controller = require('./node/data_controller')(express.Router(), express.Router(), express.Router(), express.Router(), express.Router(), express.Router(), express.Router(), express.Router(), express.Router(), express.Router(), express.Router(), express.Router(), express.Router(), express.Router(), express.Router(), express.Router());
const eventProxy = require('./node/event_s2s_proxy')(express.Router(), loggingController.save_optimization_log_db);

expressApp.use(bodyParser.urlencoded({extended: true}));
expressApp.use(bodyParser.json());
expressApp.use(logger('dev'));
expressApp.use(cookieParser());
expressApp.use(cors());
expressApp.use(helmet());

expressApp.use('/getOptimizationLog', loggingController.router1)
expressApp.use('/getUsersLogList', loggingController.router2)
expressApp.use('/getUserLog', loggingController.router3)

expressApp.use('/getDomain', urlGetter.router);
expressApp.use('/addApp', data_controller.router1);
expressApp.use('/removeApp', data_controller.router7);
expressApp.use('/getApps', data_controller.router2);
expressApp.use('/setAppConfigs', data_controller.router3);
expressApp.use('/getAppConfigs', data_controller.router4);
expressApp.use('/setDefaultTrackerUrl', data_controller.router5);
expressApp.use('/getDefaultTrackerUrl', data_controller.router6);
expressApp.use('/setConversionData', appsflyerPushApi.router1);
expressApp.use('/getDomainSpeed', appsflyerPushApi.router2);
expressApp.use('/sendEvent', eventProxy);

expressApp.use('/getSubIdInstalls', data_controller.router8);
expressApp.use('/clearSubIdInstalls', data_controller.router9);

expressApp.use('/setSubIdTracker', data_controller.router10);
expressApp.use('/pauseSubIdTracker', data_controller.router13);
expressApp.use('/resumeSubIdTracker', data_controller.router14);
expressApp.use('/getSubIdTrackerCached', data_controller.router15);
expressApp.use('/getSubIdTracker', data_controller.router12);
expressApp.use('/clearSubIdTracker', data_controller.router11);
expressApp.use('/getIp', data_controller.router16);


expressApp.use(function (err, req, res, next) {
    res.send((err === null) ? 'Something broke!' : err);
});

const workers = process.env.WORKERS || require('os').cpus().length;
// init the server
if (cluster.isMaster) {


    console.log('start cluster with %s workers', workers);

    for (var i = 0; i < workers; ++i) {
        const worker = cluster.fork().process;
        console.log('worker %s started.', worker.pid);
    }

    cluster.on('exit', function (worker) {
        console.log('worker %s stop.', worker.process.pid);
        cluster.fork();
    });
} else {
    appsflyerPushApi.initCron()
    loggingController.initCron()

    http.createServer(expressApp)
        .listen(portHttp, function () {
            console.log('listening http on port ' + portHttp + '! Go to http://' + 'node' + ':' + portHttp);
        });

    https.createServer({
        key: fs.readFileSync('ssl/server.key'),
        cert: fs.readFileSync('ssl/server.cert')
    }, expressApp)
        .listen(portHttps, function () {
            console.log('listening https on port ' + portHttps + '! Go to https://' + 'node' + ':' + portHttps);
        });
}