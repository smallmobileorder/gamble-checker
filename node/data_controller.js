module.exports = (router1, router2, router3, router4,
                  router5, router6, router7, router8,
                  router9, router10, router11, router12,
                  router13, router14, router15, router16) => {
    router1.post('/', add_app);
    router2.get('/', get_apps);
    router3.post('/', set_app_configs);
    router4.get('/', get_app_configs);
    router5.post('/', set_default_tracker_url);
    router6.get('/', get_default_tracker_url);
    router7.post('/', remove_app);
    router8.get('/', get_subid_unics);
    router9.post('/', clear_subid_unics);
    router10.post('/', set_subid_tracker_url);
    router11.post('/', delete_subid_tracker_url);
    router12.get('/', get_subid_tracker_url);
    router13.post('/', pause_sub_id_tracker_url);
    router14.post('/', resume_sub_id_tracker_url);
    router15.get('/', get_subid_tracker_url_cached);
    router16.get('/', get_req_ipi);
    router16.post('/', get_req_ipi);
    return {
        router1: router1,
        router2: router2,
        router3: router3,
        router4: router4,
        router5: router5,
        router6: router6,
        router7: router7,
        router8: router8,
        router9: router9,
        router10: router10,
        router11: router11,
        router12: router12,
        router13: router13,
        router14: router14,
        router15: router15,
        router16: router16,
    }
};

const url = require('url');
const log4js = require("log4js");
const logger = log4js.getLogger("Data controller");
const db_helper = require('./db_helper')

function get_req_ipi(req, res) {
    const queryObject = url.parse(req.url,true).query;
    const forwarded = req.headers['x-forwarded-for']
    const ip = forwarded ? forwarded.split(/, /)[0] : req.connection.remoteAddress
    res.status(200).json({response: `Your ip: ${ip}`});
}

function set_subid_tracker_url(req, res) {
    const queryObject = url.parse(req.url,true).query;

    logger.level = "trace";
    logger.trace(`Set sub id tracker: ${queryObject.bundle_back}, ${queryObject.sub_id}, ${queryObject.tracker_url}`);

    db_helper().set_sub_id_tracker_url(queryObject.bundle_back, queryObject.sub_id, queryObject.tracker_url)

    res.status(200).json({response: 'Success'});
}

function pause_sub_id_tracker_url(req, res) {
    const queryObject = url.parse(req.url,true).query;

    logger.level = "trace";
    logger.trace(`Pause sub id tracker: ${queryObject.bundle_back}, ${queryObject.sub_id}`);

    db_helper().pause_sub_id_tracker_url(queryObject.bundle_back, queryObject.sub_id)

    res.status(200).json({response: 'Success'});
}

function resume_sub_id_tracker_url(req, res) {
    const queryObject = url.parse(req.url,true).query;

    logger.level = "trace";
    logger.trace(`Resume sub id tracker: ${queryObject.bundle_back}, ${queryObject.sub_id}`);

    db_helper().resume_sub_id_tracker_url(queryObject.bundle_back, queryObject.sub_id)
    res.status(200).json({response: 'Success'});
}

function get_subid_tracker_url(req, res) {
    const queryObject = url.parse(req.url,true).query;

    logger.level = "trace";
    logger.trace(`Get sub id tracker: ${queryObject.bundle_back}, ${queryObject.sub_id}`);

    db_helper().get_sub_id_tracker_url(queryObject.bundle_back, queryObject.sub_id, (tracker_url) => {
        res.status(200).json({tracker_url: tracker_url});
    })
}

function get_subid_tracker_url_cached(req, res) {
    const queryObject = url.parse(req.url,true).query;

    logger.level = "trace";
    logger.trace(`Get sub id tracker cached: ${queryObject.bundle_back}, ${queryObject.sub_id}`);

    db_helper().get_sub_id_tracker_url_cached(queryObject.bundle_back, queryObject.sub_id, (tracker_url) => {
        res.status(200).json({tracker_url: tracker_url});
    })
}

function delete_subid_tracker_url(req, res) {
    const queryObject = url.parse(req.url,true).query;

    logger.level = "trace";
    logger.trace(`Delete sub id tracker: ${queryObject.bundle_back}, ${queryObject.sub_id}`);

    db_helper().delete_sub_id_tracker_url(queryObject.bundle_back, queryObject.sub_id)
    res.status(200).json({response: 'Success'});
}

function get_subid_unics(req, res) {
    const queryObject = url.parse(req.url,true).query;

    logger.level = "trace";
    logger.trace(`Get sub id unics: ${queryObject.sub_id}`);

    db_helper().get_sub1_unic_installs(queryObject.sub_id, (unics) => {
        res.status(200).json({unics: unics});
    })
}

function clear_subid_unics(req, res) {
    const queryObject = url.parse(req.url,true).query;

    logger.level = "trace";
    logger.trace(`Clear sub id unics: ${queryObject.sub_id}`);

    db_helper().clear_sub1_unic_installs(queryObject.sub_id)
    res.status(200).json({status: "Success"});
}

function add_app(req, res) {
    const queryObject = url.parse(req.url,true).query;

    logger.level = "trace";
    logger.trace(`Add app: ${queryObject.bundle_id}`);

    db_helper().add_app(queryObject.bundle_id, (resp) => {
        res.status(200).json({response: resp});
    })
}

function remove_app(req, res) {
    const queryObject = url.parse(req.url,true).query;

    logger.level = "trace";
    logger.trace(`Remove app: ${queryObject.bundle_id}`);

    db_helper().remove_app(queryObject.bundle_id, (resp) => {
        res.status(200).json({response: resp});
    })
}

function get_apps(req, res) {
    const queryObject = url.parse(req.url,true).query;

    logger.level = "trace";
    logger.trace(`Get apps list`);

    db_helper().get_apps_list((resp) => {
        res.status(200).json({apps: resp});
    })
}

function set_app_configs(req, res) {
    const queryObject = url.parse(req.url,true).query;

    logger.level = "trace";
    logger.trace(`Set app config: ${queryObject.bundle_id}`);

    let hostAdded = false, devKeyAdded = false, geoAdded = false, jsonAdded = false, trackerAdded = false, openInWVAdded = false, filterVpn = false
    const ready_check = () => {
        if (jsonAdded && geoAdded && trackerAdded && openInWVAdded && filterVpn && hostAdded && devKeyAdded)
            res.status(200).json({response: 'Success'});
    }
    if (queryObject.host_name !== undefined && queryObject.appsflyer_dev_key !== undefined)
    {
        db_helper().add_app_host_name_and_dev_key(queryObject.bundle_id, queryObject.app_id, queryObject.host_name, queryObject.appsflyer_dev_key, (resp) => {
            hostAdded = true
            devKeyAdded = true
            ready_check()
        })
    } else {
        hostAdded = true
        devKeyAdded = true
    }
    if (queryObject.open_in_wv === 'true' || queryObject.open_in_wv === 'false')
    {
        db_helper().add_app_open_in_wv(queryObject.bundle_id, queryObject.open_in_wv, (resp) => {
            openInWVAdded = true
            ready_check()
        })
    }
    else
        openInWVAdded = true
    if (queryObject.filter_vpn === 'true' || queryObject.filter_vpn === 'false')
    {
        db_helper().add_app_filter_vpn(queryObject.bundle_id, queryObject.filter_vpn, (resp) => {
            filterVpn = true
            ready_check()
        })
    }
    else
        filterVpn = true
    if (queryObject.tracker_url_1 != null &&
        is_uri(queryObject.tracker_url_1))
    {
        let tracker_url_2 = queryObject.tracker_url_1
        if (queryObject.tracker_url_2 != null &&
            is_uri(queryObject.tracker_url_2))
            tracker_url_2 = queryObject.tracker_url_2
        db_helper().add_app_tracker_url(
            queryObject.bundle_id,
            queryObject.tracker_url_1,
            tracker_url_2,
            (resp) => {
                trackerAdded = true
                ready_check()
            }
        )
    }
    else
        trackerAdded = true
    db_helper().add_app_geo_split(queryObject.bundle_id, queryObject.geo_split, (resp) => {
        geoAdded = true
        ready_check()
    })
    db_helper().add_app_json_template(queryObject.bundle_id, queryObject.json_template, (resp) => {
        jsonAdded = true
        ready_check()
    })
}

function get_app_configs(req, res) {
    const queryObject = url.parse(req.url,true).query;

    logger.level = "trace";
    logger.trace(`Get app config: ${queryObject.bundle_id}`);

    let app_id = null, host_name = null, dev_key = null, geoAdded = null, jsonAdded = null, trackerAdded_1 = null, trackerAdded_2 = null, openInWVAdded = null, filterVpn = null
    const ready_check = () => {
        if (host_name != null && dev_key != null && jsonAdded != null && geoAdded != null && trackerAdded_1 != null && trackerAdded_2 != null && openInWVAdded != null && filterVpn != null)
        {
            const json_result = {
                geo_split: geoAdded,
                json_template: jsonAdded,
                tracker_url_1: trackerAdded_1,
                tracker_url_2: trackerAdded_2,
                open_in_wv: openInWVAdded,
                filter_vpn: filterVpn,
                host_name: host_name,
                appsflyer_dev_key: dev_key,
                app_id: app_id,
            }
            res.status(200).json(json_result);
        }
    }
    db_helper().get_app_host_name_and_dev_key(queryObject.bundle_id, (host_, dev_, app_id_) => {
        host_name = host_
        dev_key = dev_
        app_id = app_id_
        ready_check()
    })
    db_helper().get_app_open_in_wv(queryObject.bundle_id, (resp) => {
        openInWVAdded = resp
        ready_check()
    })
    db_helper().get_app_filter_vpn(queryObject.bundle_id, (resp) => {
        filterVpn = resp
        ready_check()
    })
    db_helper().get_app_tracker_url(queryObject.bundle_id, (resp_1, resp_2) => {
        trackerAdded_1 = resp_1
        trackerAdded_2 = resp_2
        ready_check()
    })
    db_helper().get_app_geo_split(queryObject.bundle_id, (resp) => {
        geoAdded = resp
        ready_check()
    })
    db_helper().get_app_json_template(queryObject.bundle_id, (resp) => {
        jsonAdded = JSON.parse(resp)
        ready_check()
    })
}

function set_default_tracker_url(req, res) {
    const queryObject = url.parse(req.url,true).query;
    if (queryObject.tracker_url_1 != null && is_uri(queryObject.tracker_url_1))
    {
        let tracker_url_2 = queryObject.tracker_url_1
        if (queryObject.tracker_url_2 != null && is_uri(queryObject.tracker_url_2))
            tracker_url_2 = queryObject.tracker_url_2

        logger.level = "trace";
        logger.trace(`Set default trackers: ${queryObject.tracker_url_1}, ${tracker_url_2}`);

        db_helper().add_app_default_tracker_url(queryObject.tracker_url_1, tracker_url_2, (resp) => {
            res.status(200).json({response: resp});
        })
    } else {
        logger.trace(`Set default tracker: url 1 not valid`);

        res.status(404).json({response: 'Not valid url'});
    }
}

function get_default_tracker_url(req, res) {
    logger.level = "trace";
    logger.trace(`Get default trackers`);

    db_helper().get_app_default_tracker_url((resp1, resp2) => {
        res.status(200).json({
            tracker_url_1: resp1,
            tracker_url_2: resp2
        });
    })
}

function is_uri(url) {
    try {
        new URL(url)
        return true
    } catch (e) {
        console.log(e.toString())
        return false
    }
}
