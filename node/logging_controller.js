module.exports = (router1, router2, router3) => {
    router1.get('/', get_optimization_log);
    router2.get('/', get_user_routing_log);
    router3.get('/', get_selected_user_routing_log);
    return {
        router1: router1,
        router2: router2,
        router3: router3,
        save_optimization_log_db: save_optimization_log_db,
        save_user_log_db: save_user_log_db,
        initCron: initCron
    }
}

const url = require('url');
const redisClient = require('./redis-client')

process.env.TZ = 'Europe/Moscow'

const defaultPageSize = 100

const table_optimization_log = "table_optimization_"
const table_users_log = "table_users_"

const last_index_param = "last_index_par_"
const time_stamp_param = "timeStamp"

const event_name_param = "eventName"
const event_value_param = "eventValue"
const bundle_id_param = "bundleId"
const dev_key_param = "devKey"
const host_param = "host"
const appsflyer_id_param = "appsflyerId"

const bundle_back_param = "bundle_back"
const campaign_param = "campaign"
const filterValue_param = "filterValue"
const device_name_param = "device_name"
const geo_param = "geo"
const fromPhone_param = "fromPhone"
const filtersResult_param = "filtersResult"
const direction_param = "direction"

// 0 0 0 * * *
function initCron() {
    const cron = require('node-cron');
    console.log('Init cron log')
    cron.schedule('0 0 10 * * *', async () => {
        const dateTime = Date.now();
        const timeMinusDays = dateTime - 1000 * 60 * 60 * 24 * 2;

        console.log('start clearing log...')
        let i = 0
        let bundleID
        let iStr = await redisClient.getAsync(table_users_log + last_index_param)
        if (iStr === undefined || iStr == null)
            return
        else
            i = parseInt(iStr)

        do {
            bundleID = await redisClient.getAsync(table_users_log + bundle_back_param + i.toString())
            if (bundleID !== null && bundleID !== undefined) {
                const timeStamp = await redisClient.getAsync(table_users_log + time_stamp_param + i.toString())
                if (Date.parse(timeStamp) < timeMinusDays) {
                    redisClient.client.del(table_users_log + bundle_back_param + i.toString())
                    redisClient.client.del(table_users_log + campaign_param + i.toString())
                    redisClient.client.del(table_users_log + filterValue_param + i.toString())
                    redisClient.client.del(table_users_log + device_name_param + i.toString())
                    redisClient.client.del(table_users_log + geo_param + i.toString())
                    redisClient.client.del(table_users_log + fromPhone_param + i.toString())
                    redisClient.client.del(table_users_log + filtersResult_param + i.toString())
                    redisClient.client.del(table_users_log + direction_param + i.toString())
                    redisClient.client.del(table_users_log + time_stamp_param + i.toString())
                }
            }
            i--
        } while (bundleID !== null && bundleID !== undefined)

        i = parseInt(iStr)
        do {
            bundleID = await redisClient.getAsync(table_optimization_log + bundle_back_param + i.toString())
            if (bundleID !== null && bundleID !== undefined) {
                const timeStamp = await redisClient.getAsync(table_optimization_log + time_stamp_param + i.toString())
                if (Date.parse(timeStamp) < timeMinusDays) {
                    redisClient.client.del(table_optimization_log + time_stamp_param + i.toString())
                    redisClient.client.del(table_optimization_log + event_name_param + i.toString())
                    redisClient.client.del(table_optimization_log + event_value_param + i.toString())
                    redisClient.client.del(table_optimization_log + bundle_id_param + i.toString())
                    redisClient.client.del(table_optimization_log + dev_key_param + i.toString())
                    redisClient.client.del(table_optimization_log + host_param + i.toString())
                    redisClient.client.del(table_optimization_log + appsflyer_id_param + i.toString())
                }
            }
            i--
        } while (bundleID !== null && bundleID !== undefined)
        console.log('finish clearing log...')
    }, null);
}

async function save_user_log_db(geo, fromPhone, filtersResult, direction) {
    const lastIndex = await redisClient.getAsync(table_users_log + last_index_param)
    let lastIndexInt = !isNumeric(lastIndex) ? 0 : parseInt(lastIndex) + 1
    redisClient.setAsync(table_users_log + last_index_param, lastIndexInt.toString())

    const date_string = new Date().toLocaleString()
    redisClient.setAsync(table_users_log + time_stamp_param + lastIndexInt.toString(), date_string)
    redisClient.setAsync(table_users_log + bundle_back_param + lastIndexInt.toString(), fromPhone.bundle_back === undefined ? "undefined" : fromPhone.bundle_back)
    redisClient.setAsync(table_users_log + campaign_param + lastIndexInt.toString(), fromPhone.campaign === undefined ? "undefined" : fromPhone.campaign)
    redisClient.setAsync(table_users_log + filterValue_param + lastIndexInt.toString(), filtersResult.value)
    redisClient.setAsync(table_users_log + device_name_param + lastIndexInt.toString(), fromPhone.device_name === undefined ? "undefined" : fromPhone.device_name)
    redisClient.setAsync(table_users_log + geo_param + lastIndexInt.toString(), geo)
    redisClient.setAsync(table_users_log + fromPhone_param + lastIndexInt.toString(), JSON.stringify(fromPhone))
    redisClient.setAsync(table_users_log + filtersResult_param + lastIndexInt.toString(), JSON.stringify(filtersResult))
    redisClient.setAsync(table_users_log + direction_param + lastIndexInt.toString(), JSON.stringify(direction))
}

async function get_user_log_db(bundle_id, index, amount, filterKey, filterValue, callback) {
    const responseArr = []
    let bundleID = undefined
    const maxAmount = amount < 0 ? -1 * amount : amount
    let currentAmount = 0
    let i = 0
    let iStr = isNumeric(index) ? index : (await redisClient.getAsync(table_users_log + last_index_param))
    if (iStr === undefined || iStr == null)
    {
        callback(responseArr, 0)
        return
    }
    else
        i = parseInt(iStr)

    do {
        if (amount < 0)
            i++
        else
            i--
        bundleID = await redisClient.getAsync(table_users_log + bundle_back_param + i.toString())
        let filterProcessed = true
        if (bundleID !== null)
        {
            if (bundleID === bundle_id) {
                if (filterKey !== undefined && filterValue !== undefined)
                {
                    const filterProcessedStr = await redisClient.getAsync(table_users_log + filterKey.toString() + i.toString())
                    if (filterProcessedStr === undefined || filterProcessedStr == null)
                        filterProcessed = false
                    else
                        filterProcessed = filterProcessedStr.includes(filterValue)
                }
                if (filterProcessed)
                    currentAmount++
            }
        }
        if (bundleID === bundle_id && currentAmount <= maxAmount && filterProcessed) {
            const campaign_it = await redisClient.getAsync(table_users_log + campaign_param + i.toString())
            const filterValue_it = await redisClient.getAsync(table_users_log + filterValue_param + i.toString())
            const device_name_it = await redisClient.getAsync(table_users_log + device_name_param + i.toString())
            const geo_it = await redisClient.getAsync(table_users_log + geo_param + i.toString())
            const timeStamp = await redisClient.getAsync(table_users_log + time_stamp_param + i.toString())
            const direction_it = await redisClient.getAsync(table_users_log + direction_param + i.toString())
            const item = {
                userLogId: i.toString(),
                bundle_back: bundleID,
                campaign: campaign_it,
                filterValue: filterValue_it,
                direction: direction_it,
                timeStamp: timeStamp
                    .replace(/T/, ' ')
                    .replace(/\..+/, ''),
                device_name: device_name_it,
                geo: geo_it
            }
            if (amount < 0)
                responseArr.unshift(item)
            else
                responseArr.push(item)
        }
    } while (bundleID !== null && bundleID !== undefined)

    if (amount < 0)
    {
        i = parseInt(iStr)
        do {
            bundleID = await redisClient.getAsync(table_users_log + bundle_back_param + i.toString())
            let filterProcessed = true
            if (bundleID !== null)
            {
                if (bundleID === bundle_id) {
                    if (filterKey !== undefined && filterValue !== undefined)
                    {
                        const filterProcessedStr = await redisClient.getAsync(table_users_log + filterKey.toString() + i.toString())
                        if (filterProcessedStr === undefined || filterProcessedStr == null)
                            filterProcessed = false
                        else
                            filterProcessed = filterProcessedStr.includes(filterValue)
                    }
                    if (filterProcessed)
                        currentAmount++
                }
            }
            i--
        } while (bundleID !== null && bundleID !== undefined)
    }

    callback(responseArr, currentAmount - responseArr.length)
}

async function save_optimization_log_db(eventName, eventValue, bundleId,
                                        devKey, host, appsflyerUid) {
    const lastIndex = await redisClient.getAsync(table_optimization_log + last_index_param)
    let lastIndexInt = !isNumeric(lastIndex) ? 0 : parseInt(lastIndex) + 1
    redisClient.setAsync(table_optimization_log + last_index_param, lastIndexInt.toString())

    const date_string = new Date().toLocaleString()
    redisClient.setAsync(table_optimization_log + time_stamp_param + lastIndexInt.toString(), date_string)
    redisClient.setAsync(table_optimization_log + event_name_param + lastIndexInt.toString(), eventName)
    redisClient.setAsync(table_optimization_log + event_value_param + lastIndexInt.toString(), eventValue)
    redisClient.setAsync(table_optimization_log + bundle_id_param + lastIndexInt.toString(), bundleId)
    redisClient.setAsync(table_optimization_log + dev_key_param + lastIndexInt.toString(), devKey)
    redisClient.setAsync(table_optimization_log + host_param + lastIndexInt.toString(), host)
    redisClient.setAsync(table_optimization_log + appsflyer_id_param + lastIndexInt.toString(), appsflyerUid)
}

async function get_optimization_log_db(bundle_id, index, amount, filterKey, filterValue, callback) {
    const responseArr = []
    const eventGlobalStatisticArr = []
    const eventGlobalStatistic = {}
    let bundleID = undefined
    const maxAmount = amount < 0 ? -1 * amount : amount
    let currentAmount = 0
    let i = 0
    let iStr = isNumeric(index) ? index : await redisClient.getAsync(table_optimization_log + last_index_param)
    if (iStr === undefined || iStr == null)
    {
        callback(responseArr, 0, eventGlobalStatisticArr)
        return
    }
    else
        i = parseInt(iStr)

    do {
        if (amount < 0)
            i++
        else
            i--
        bundleID = await redisClient.getAsync(table_optimization_log + bundle_id_param + i.toString())
        let eventName
        let eventValue
        let filterProcessed = true
        if (bundleID !== null)
        {
            if (bundleID === bundle_id) {
                if (filterKey !== undefined && filterValue !== undefined)
                {
                    const filterProcessedStr = await redisClient.getAsync(table_optimization_log + filterKey.toString() + i.toString())
                    if (filterProcessedStr === undefined || filterProcessedStr == null)
                        filterProcessed = false
                    else
                        filterProcessed = filterProcessedStr.includes(filterValue)
                }
                if (filterProcessed)
                {
                    currentAmount++
                    eventName = await redisClient.getAsync(table_optimization_log + event_name_param + i.toString())
                    eventValue = await redisClient.getAsync(table_optimization_log + event_value_param + i.toString())

                    if (eventGlobalStatistic[eventName] === undefined)
                    {
                        eventGlobalStatistic[eventName] = {
                            count: "0",
                            valueTotal: "0"
                        }
                    }
                    eventGlobalStatistic[eventName].count = (parseInt(eventGlobalStatistic[eventName].count)+1).toString()
                    if (isNumeric(eventValue) && eventValue !== "0")
                        eventGlobalStatistic[eventName].valueTotal = (parseInt(eventGlobalStatistic[eventName].valueTotal) + parseInt(eventValue)).toString()
                }
            }
        }
        if (bundleID === bundle_id && currentAmount <= maxAmount && filterProcessed) {
            const devKey = await redisClient.getAsync(table_optimization_log + dev_key_param + i.toString())
            const host = await redisClient.getAsync(table_optimization_log + host_param + i.toString())
            const timeStamp = await redisClient.getAsync(table_optimization_log + time_stamp_param + i.toString())
            const appsflyerId = await redisClient.getAsync(table_optimization_log + appsflyer_id_param + i.toString())
            const item = {
                eventId: i.toString(),
                eventName: eventName,
                eventValue: eventValue,
                bundleId: bundleID,
                devKey: devKey,
                host: host,
                timeStamp: timeStamp
                    .replace(/T/, ' ')
                    .replace(/\..+/, ''),
                appsflyerId: appsflyerId
            }
            if (amount < 0)
                responseArr.unshift(item)
            else
                responseArr.push(item)
        }
    } while (bundleID !== null && bundleID !== undefined)

    if (amount < 0)
    {
        i = parseInt(iStr)
        do {
            bundleID = await redisClient.getAsync(table_optimization_log + bundle_id_param + i.toString())
            let filterProcessed = true
            if (bundleID !== null)
            {
                if (bundleID === bundle_id) {
                    if (filterKey !== undefined && filterValue !== undefined)
                    {
                        const filterProcessedStr = await redisClient.getAsync(table_optimization_log + filterKey.toString() + i.toString())
                        if (filterProcessedStr === undefined || filterProcessedStr == null)
                            filterProcessed = false
                        else
                            filterProcessed = filterProcessedStr.includes(filterValue)
                    }
                    if (filterProcessed)
                        currentAmount++
                }
            }
            i--
        } while (bundleID !== null && bundleID !== undefined)
    }

    for (let egKey in eventGlobalStatistic) {
        eventGlobalStatisticArr.push({
            name: egKey,
            count: eventGlobalStatistic[egKey].count,
            valueTotal: eventGlobalStatistic[egKey].valueTotal
        })
    }
    callback(responseArr, currentAmount - responseArr.length, eventGlobalStatisticArr)
}

function get_optimization_log(req, res) {
    const queryObject = url.parse(req.url,true).query;

    if (queryObject.bundle_back === undefined)
    {
        res.status(400).json({
            message: "No bundle id detected"
        });
        return
    }

    let packageSize = defaultPageSize
    if (queryObject.packageSize !== undefined)
        packageSize = queryObject.packageSize

    get_optimization_log_db(queryObject.bundle_back, queryObject.startIndex, packageSize, queryObject.filterKey, queryObject.filterValue,
        (respData, leftAmount, eventGlobalStatisticArr) => {
            res.status(200).json({
                data: respData,
                dataFullSize: leftAmount.toString(),
                eventsCounts: eventGlobalStatisticArr
            });
        })
}

function get_user_routing_log(req, res) {
    const queryObject = url.parse(req.url,true).query;

    if (queryObject.bundle_back === undefined)
    {
        res.status(400).json({
            message: "No bundle id detected"
        });
        return
    }

    let packageSize = defaultPageSize
    if (queryObject.packageSize !== undefined)
        packageSize = queryObject.packageSize

    get_user_log_db(queryObject.bundle_back, queryObject.startIndex, packageSize, queryObject.filterKey, queryObject.filterValue,
        (respData, leftAmount) => {
            res.status(200).json({
                data: respData,
                dataFullSize: leftAmount.toString()
            });
        })
}

async function get_selected_user_routing_log(req, res) {
    const queryObject = url.parse(req.url,true).query;
    if (!isNumeric(queryObject.userLogId))
        res.status(400).json({
            message: "No user id detecred"
        });
    else {
        const i = parseInt(queryObject.userLogId)
        const bundleID = await redisClient.getAsync(table_users_log + bundle_back_param + i.toString())
        const campaign_it = await redisClient.getAsync(table_users_log + campaign_param + i.toString())
        const filterValue_it = await redisClient.getAsync(table_users_log + filterValue_param + i.toString())
        const device_name_it = await redisClient.getAsync(table_users_log + device_name_param + i.toString())
        const geo_it = await redisClient.getAsync(table_users_log + geo_param + i.toString())
        const fromPhone_it = await redisClient.getAsync(table_users_log + fromPhone_param + i.toString())
        const filtersResult_it = await redisClient.getAsync(table_users_log + filtersResult_param + i.toString())
        const direction_it = await redisClient.getAsync(table_users_log + direction_param + i.toString())
        const timeStamp = await redisClient.getAsync(table_users_log + time_stamp_param + i.toString())
        res.status(200).json({
            item: {
                userLogId: queryObject.userLogId,
                bundle_back: bundleID,
                campaign: campaign_it,
                filterValue: filterValue_it,
                timeStamp: timeStamp
                    .replace(/T/, ' ')
                    .replace(/\..+/, ''),
                device_name: device_name_it,
                geo: geo_it,
                fromPhone: fromPhone_it,
                filtersResult: filtersResult_it,
                direction: direction_it
            }
        });
    }
}

function isNumeric(str) {
    if (str === undefined || str == null) return false
    if (typeof str != "string") return false // we only process strings!
    return !isNaN(str) && // use type coercion to parse the _entirety_ of the string (`parseFloat` alone does not do this)...
        !isNaN(parseFloat(str)) // ...and ensure strings of whitespace fail
}
