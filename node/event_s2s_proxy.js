let save_optimization_log_db

module.exports = (router, save_optimization_log_db_) => {
    save_optimization_log_db = save_optimization_log_db_
    router.get('/', send_event);
    router.post('/', send_event);
    return router
};

const url = require('url');
const request = require("request");
const db_helper = require('./db_helper')

function send_event(req, res) {
    const queryObject = url.parse(req.url,true).query;
    let host = req.get('host');
    console.log(host)
    db_helper().get_host_dev_key_and_bundle(host, (devKey, bundleId) => {
        save_optimization_log_db(
            queryObject.event_name,
            queryObject.event_value,
            bundleId,
            devKey,
            host,
            queryObject.appsflyer_id
        )
        let options = {
            method: 'POST',
            url: 'https://api2.appsflyer.com/inappevent/' + bundleId,
            headers:
                {
                    "authentication": devKey,
                    'Content-Type': 'application/json'
                },
            body:
                {
                    appsflyer_id: queryObject.appsflyer_id,
                    eventName: queryObject.event_name,
                    eventValue: queryObject.event_value
                },
            json: true
        };
        request(options, function (error, response, body) {
            if (error) throw new Error(error);

            res.status(200).json({response: response, body: body, error: error});
        });
    })
}
