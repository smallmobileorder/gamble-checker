let get_offers_response

module.exports = (router1, router2, get_offers_response_) => {
    router1.post('/', set_conversion_data);
    router2.get('/', get_url);
    get_offers_response = get_offers_response_
    return {
        router1: router1,
        router2: router2,
        initCron: initCron
    }
};

const url = require('url');
const log4js = require("log4js");
const logger = log4js.getLogger("Appsflyer push api");

let data_cache = []

function initCron() {
    logger.level = "trace";
    logger.trace('Init Cron push api');

    const cron = require('node-cron');
    cron.schedule('* 1 * * * *', () => {
        const dateTime = Date.now();
        const timeMinusOneHour = dateTime - 1000 * 60 * 60; //1 час
        data_cache.forEach(row => console.log(row));
        data_cache = data_cache.filter(function (row) {
            if (row.time === undefined) return false;
            const installTime = Date.parse(row.time);
            return installTime > timeMinusOneHour;
        })
    }, null);

}

function set_conversion_data(req, res) {
    logger.level = "trace";
    console.trace('Set appsflyer push api conversion data')

    const queryObject = url.parse(req.url,true).query;

    logger.level = "info";
    logger.info('Data cache before:\n%s', data_cache)

    data_cache.push({
        time: Date.now(),
        data: queryObject
    })

    logger.info('Data cache after:\n%s', data_cache)

    res.status(200).send();
}

function get_index(apps_uid, gaid) {
    for (let i = 0; i < data_cache.length; i++) {
        if (data_cache[i].data.appsflyer_id === apps_uid || data_cache[i].data.advertising_id === gaid) {
            return i
        }
    }
    return null
}

function get_url(req, res) {
    try {
        logger.level = "trace";
        logger.trace('Get from appsflyer push api conversion data tracker url')

        const queryObject = url.parse(req.url,true).query;
        const user_data = JSON.parse(Buffer.from(queryObject.encoded_data, 'base64').toString())
        const index = get_index(user_data.appsflyer_uid, user_data.gaid)

        if (index != null) {
            logger.level = "info";
            logger.info('Data cache before:\n%s', data_cache)

            const fetched_data = data_cache[index].data
            data_cache.splice(index, 1)

            logger.info('Data cache after:\n%s', data_cache)

            if (fetched_data.media_source != null) {
                const forwarded = req.headers['x-forwarded-for']
                const ip = forwarded ? forwarded.split(/, /)[0] : req.connection.remoteAddress

                logger.level = "trace";
                logger.trace('Fetched data found')

                get_offers_response(ip, queryObject.encoded_data, fetched_data, (response) => {
                    if (response != null)
                        res.status(200).json(response);
                    else
                        res.status(200).json({});
                })
            } else
            {
                logger.trace('Fetched data not found')

                res.status(200).json({});
            }
        } else
        {
            logger.trace('Conversion data not found')

            res.status(200).json({});
        }
    } catch (e) {
        logger.level = "trace";
        logger.error(e.toString())
    }
}
