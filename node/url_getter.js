let save_user_log_db

module.exports = (router, save_user_log_db_) => {
    save_user_log_db = save_user_log_db_
    router.get('/', get_url);
    return {
        router: router,
        get_offers_response: get_offers_response
    }
};

const url = require('url');
const maxmind = require('maxmind');

let lookup = null
maxmind.open('./data/GeoLite2-Country.mmdb').then((look) => {
    lookup = look
});

const offers_helper = require('./offers_logic')
const response_helper = require('./response_linker')
const filter_helper = require('./filter_logic')

function get_url(req, res) {
    try {
        const queryObject = url.parse(req.url,true).query;
        const forwarded = req.headers['x-forwarded-for']
        const ip = forwarded ? forwarded.split(/, /)[0] : req.connection.remoteAddress

        get_offers_response(ip, queryObject.encoded_data, null, (response) => {
            if (response != null)
                res.status(200).json(response);
            else
                res.status(200).json({});
        })
    } catch (e) {
        console.log(e.toString())
    }
}

function get_offers_response(ip, user_data_encoded, fetched_naming, callback) {
    const user_data = parse_user_data(decode_user_params(user_data_encoded), user_data_encoded)
    response_helper().process_and_add_naming(user_data)
    if (fetched_naming != null)
        response_helper().process_and_add_naming(fetched_naming)
    offers_helper().get_filter_vpn(user_data.bundle_back, (filter_vpn) => {
        const filter_bool = filter_helper().filter_user_data(filter_vpn, user_data, fetched_naming)
        const geo_obj = lookup != null ? lookup.get(ip) : null;
        let geo = null
        if (geo_obj != null)
            geo = geo_obj.country.iso_code
        if (filter_bool.value)
        {
            offers_helper().get_sub_id_offer(user_data, fetched_naming, (tracker_url) => {
                offers_helper().get_open_in_wv(user_data.bundle_back, (open_in_wv) => {
                    if (tracker_url != null) {
                        console.log('Returned tracker')
                        console.log(tracker_url)
                        const direction_json = response_helper().generate_response(user_data, fetched_naming, tracker_url, tracker_url, open_in_wv, callback);
                        save_user_log_db(geo, user_data, filter_bool, direction_json)
                    } else {
                        offers_helper().get_splited_offer(user_data, fetched_naming, geo, (offers) => {
                            console.log('Returned offers')
                            console.log(offers)
                            if (offers != null)
                            {
                                let open_wv_ans = open_in_wv
                                if (offers.open_browser !== undefined)
                                    open_wv_ans = !(offers.open_browser === "true")
                                const direction_json = response_helper().generate_response(user_data, fetched_naming, offers.url_1, offers.url_2, open_wv_ans, callback);
                                save_user_log_db(geo, user_data, filter_bool, direction_json)
                            }
                            else
                            {
                                save_user_log_db(geo, user_data, filter_bool, {})
                                callback(null)
                            }
                        });
                    }
                })
            })
        } else
        {
            save_user_log_db(geo, user_data, filter_bool, {})
            callback(null)
        }
    })
}

function decode_user_params(str_raw) {
    let user_raw_data = Buffer.from(str_raw, 'base64').toString()
    if (isJson(user_raw_data))
        return user_raw_data
    else {
        console.log('Replace spaces, json not valid, of:')
        console.log(user_raw_data)
        console.log('After:')
        const urd_after = Buffer.from(str_raw.replace(/\s/g, 'e'), 'base64').toString()
        console.log(urd_after)
        return urd_after
    }
}

function parse_user_data(user_data, encoded_data) {
    if (!isJson(user_data))
    {
        console.log('Not valid json AGAIN:')
        console.log(user_data)
        return {
            bundle_back: 'com.broken.json',
            raw_encoded: encoded_data,
            raw_decoded: user_data
        }
    }
    return JSON.parse(user_data)
}

function isJson(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}
