module.exports = () => {
    return {
        get_splited_offer: get_splited_offer,
        get_open_in_wv: get_open_in_wv,
        get_filter_vpn: get_filter_vpn,
        get_sub_id_offer: get_sub_id_tracker_url
    };
};

const db_helper = require('./db_helper')

const random_offer_placeholder = '{random_offer}'

function get_sub_id_tracker_url(user_data, fetched_naming, callback) {
    let sub5 = null
    if (user_data != null && user_data.value5 !== undefined)
        sub5 = user_data.value5
    else if (fetched_naming != null && fetched_naming.value5 !== undefined)
        sub5 = fetched_naming.value5
    if (sub5 != null) {
        db_helper().get_sub_id_tracker_url(user_data.bundle_back, sub5, (tracker_url) => {
            if (tracker_url != null)
                db_helper().increase_sub1_unic_installs(sub5)
            callback(tracker_url)
        })
    } else
        callback(null)
}

function get_open_in_wv(bundle_id, callback) {
    db_helper().get_app_open_in_wv(bundle_id, (open_in_wv) => {
        callback(open_in_wv === 'true')
    })
}

function get_filter_vpn(bundle_id, callback) {
    db_helper().get_app_filter_vpn(bundle_id, (filter_vpn) => {
        callback(filter_vpn === 'true')
    })
}

function get_splited_offer(user_data, fetched_naming, geo, callback) {
    try {
        const bundle_id = user_data.bundle_back
        db_helper().get_app_tracker_url(bundle_id, (treker_base_url_1, treker_base_url_2) => {
            console.log('Tracker urls:')
            console.log(treker_base_url_1)
            console.log(treker_base_url_2)
            const callback_f = (resp) => {
                console.log('Geo split:')
                console.log(resp)
                if (resp != null && resp.length > 0)
                {
                    const resp_json = JSON.parse(resp)
                    const offers_split = get_splited_urls(resp_json, geo)
                    console.log('Offers and geo:')
                    console.log(geo)
                    console.log(offers_split)
                    if (offers_split != null)
                    {
                        console.log('Offers split')
                        console.log(offers_split)
                        const url1 = new URL(treker_base_url_1);
                        url1.searchParams.append('start', 'first')
                        let placeholder_exists_1 = false
                        for(let key of url1.searchParams.keys()) {
                            if (url1.searchParams.get(key) === random_offer_placeholder) {
                                placeholder_exists_1 = true
                                url1.searchParams.set(key, offers_split.offer)
                                break
                            }
                        }
                        if (!placeholder_exists_1)
                            url1.searchParams.append('p', offers_split.offer)
                        const url2 = new URL(treker_base_url_2);
                        url2.searchParams.append('start', 'second')
                        let placeholder_exists_2 = false
                        for(let key of url2.searchParams.keys()) {
                            if (url2.searchParams.get(key) === random_offer_placeholder) {
                                placeholder_exists_2 = true
                                url2.searchParams.set(key, offers_split.offer)
                                break
                            }
                        }
                        if (!placeholder_exists_2)
                            url2.searchParams.append('p', offers_split.offer)
                        callback({
                            url_1: url1.toString(),
                            url_2: url2.toString(),
                            open_browser: offers_split.open_browser
                        })
                    }
                    else
                        callback(null)
                } else
                    callback(null)
            }
            if (user_data.media_source !== undefined) {
                if (user_data.media_source.includes('unity')) {
                    db_helper().get_app_geo_split_inapp(bundle_id, callback_f)
                } else if (user_data.media_source.includes('google')) {
                    db_helper().get_app_geo_split_uac(bundle_id, callback_f)
                } else {
                    db_helper().get_app_geo_split(bundle_id, callback_f)
                }
            } else {
                db_helper().get_app_geo_split(bundle_id, callback_f)
            }
        })
    } catch (e) {
        console.log(e.toString())
        callback(null)
    }
}

function get_splited_urls(dictionary, geo) {
    for (let key in dictionary) {
        if (dictionary.hasOwnProperty(key) && key === geo) {
            return {
                offer: get_split_urls(dictionary[key]),
                open_browser: dictionary[key].open_browser
            }
        }
    }
    return null
}

function get_split_urls(dictionary) {
    let array = [];
    for(let item in dictionary) {
        if (dictionary.hasOwnProperty(item) ) {
            for(let i = 0; i < dictionary[item]; i++) {
                array.push(item);
            }
        }
    }
    return array[Math.floor(Math.random() * array.length)];
}
