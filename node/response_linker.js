module.exports = () => {
    return {
        generate_response: generate_response,
        process_and_add_naming: process_and_add_naming
    };
};

const db_helper = require('./db_helper')

const device_id_placeholder = 'device_id'

function generate_response(user_data, fetched_naming, url_1_old, url_2_old, open_in_web_view, callback) {
    const url_1 = apply_macros(url_1_old, user_data, fetched_naming)
    const url_2 = apply_macros(url_2_old, user_data, fetched_naming)
    const index1 = getRandomArbitrary(0, url_1.length)
    const index2 = getRandomArbitrary(0, url_2.length)
    db_helper().get_app_json_template(user_data.bundle_back, (template_np) => {
        const template = JSON.parse(template_np)
        const template_data = JSON.parse(template.data_template)
        template_data[template.url_1_1] = url_1.substring(0, index1)
        template_data[template.url_1_2] = url_1.substring(index1)
        template_data[template.url_2_1] = url_2.substring(0, index2)
        template_data[template.url_2_2] = url_2.substring(index2)
        template_data['override_url'] = !open_in_web_view
        callback(template_data)
    })
    return {
        url_1: url_1,
        url_2: url_2
    }
}

function process_and_add_naming(dictionary) {
    if (dictionary.campaign != null && dictionary.campaign != "null") {
        const split_res = dictionary.campaign.split("://")
        if (split_res.length <= 0 || split_res[1] === undefined) {
            console.log('empty subs')
        } else {
            const naming_subs = split_res[1].split('/')
            console.log('naming subs:')
            console.log(naming_subs)
            for (let i = 0; i < naming_subs.length; i++) {
                dictionary["value" + i.toString()] = naming_subs[i]
            }
        }
    } else if (dictionary.c != null && dictionary.c != "null") {
        const split_res = dictionary.c.split("://")
        if (split_res.length <= 0 || split_res[1] === undefined) {
            console.log('empty subs')
        } else {
            const naming_subs = split_res[1].split('/')
            console.log('naming subs:')
            console.log(naming_subs)
            for (let i = 0; i < naming_subs.length; i++) {
                dictionary["value" + i.toString()] = naming_subs[i]
            }
        }
    } else if (dictionary.deep_fb != null && dictionary.deep_fb != "null") {
        const split_res = dictionary.deep_fb.split("://")
        if (split_res.length <= 0 || split_res[1] === undefined) {
            console.log('empty subs')
        } else {
            const naming_subs = split_res[1].split('/')
            console.log('naming subs:')
            console.log(naming_subs)
            for (let i = 0; i < naming_subs.length; i++) {
                dictionary["value" + i.toString()] = naming_subs[i]
            }
        }
    }
}

function apply_macros(url, user_data, fetched_naming) {
    let url_new = url
    console.log('Macros for url:')
    console.log(url)
    const url_match = url.match(/(\{|%7B|%7b)(.*?)(\}|%7D|%7d)/g)

    for (let i in url_match) {
        const macros = url_match[i]
            .replace('{', '')
            .replace('%7B', '')
            .replace('%7b', '')
            .replace('}', '')
            .replace('%7D', '')
            .replace('%7d', '')
        console.log(macros)
        if (macros === device_id_placeholder) {
            if (user_data.appsflyer_uid != null)
                url_new = url_new.replace(url_match[i], user_data.appsflyer_uid)
            else if (fetched_naming != null && fetched_naming.appsflyer_id != null)
                url_new = url_new.replace(url_match[i], fetched_naming.appsflyer_id)
        }
        if (user_data[macros] != null)
            url_new = url_new.replace(url_match[i], user_data[macros])
        else if (fetched_naming != null && fetched_naming[macros] != null)
            url_new = url_new.replace(url_match[i], fetched_naming[macros])
    }

    return url_new
}

function getRandomArbitrary(min, max) {
    return Math.random() * (max - min) + min;
}
