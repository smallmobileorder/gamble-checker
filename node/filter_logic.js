module.exports = () => {
    return {
        filter_user_data: filter_user_data
    };
};

function filter_user_data(filter_vpn, user_data, fetched_naming) {
    let isDeep = false, isNaming = false
    if (fetched_naming != null) {
        isNaming = true
    } else {
        isDeep = user_data.deep !== undefined
        isNaming = user_data.sub_id_1 !== undefined
    }

    const bundle = filter_bundle_id(user_data.bundle_back)
    const mobile = filter_mobile_operator(user_data.mno, user_data.campaign, user_data.install_referer)
    const battery = filter_battery(
        user_data.battery_percent,
        user_data.battery_charging,
        isDeep,
        isNaming,
    )
    const phone_model = filter_phone_model(user_data.device_name)
    var vpn = user_data.device_vpn !== 'true'
    if (!filter_vpn)
        vpn = true
    return  {
        bundle: bundle,
        mobile: mobile,
        battery: battery,
        phone_model: phone_model,
        is_deep: isDeep,
        is_naming: isNaming,
        vpn: vpn,
        value: bundle && mobile && battery && phone_model && vpn
    }
}

function filter_bundle_id(bundle_id) {
    return bundle_id != null
}

function filter_mobile_operator(mno, naming, ireferer) {
    if (ireferer !== '' && ireferer !== undefined && ireferer != null && ireferer !== 'undefined' && ireferer !== 'null')
    {
        if (ireferer.includes('pcampaignid'))
            return true
    }
    if (naming === '' || naming === undefined || naming == null || naming === 'undefined' || naming === 'null')
    {
        return mno !== undefined && mno !== 'undefined' && mno !== '' && mno != null && mno !== 'null'
    }
    return true
}

function filter_battery(percent, status, is_deep, is_naming) {
    if (status === 'true' && is_deep === false && is_naming === false)
        return percent !== '100'
    return true
}

function filter_phone_model(model) {
    if (model === undefined)
        return false
    const model_lower = model.toLowerCase()
    return !(model_lower.includes('google') || model_lower.includes('nexus') || model_lower.includes('pixel'))
}
