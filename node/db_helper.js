module.exports = () => {
    return {
        add_app: add_app,
        remove_app: remove_app,
        add_app_geo_split: add_app_geo_split,
        add_app_json_template: add_app_json_template,
        get_apps_list: get_apps_list,
        get_app_geo_split: get_app_geo_split,
        get_app_geo_split_inapp: get_app_geo_split_inapp,
        get_app_geo_split_uac: get_app_geo_split_uac,
        get_app_json_template: get_app_json_template,
        add_app_tracker_url: add_app_tracker_url,
        get_app_tracker_url: get_app_tracker_url,
        add_app_default_tracker_url: add_app_default_tracker_url,
        get_app_default_tracker_url: get_app_default_tracker_url,
        add_app_open_in_wv: add_app_open_in_wv,
        get_app_open_in_wv: get_app_open_in_wv,
        add_app_filter_vpn: add_app_filter_vpn,
        get_app_filter_vpn: get_app_filter_vpn,
        add_app_host_name_and_dev_key: add_app_host_name_and_dev_key,
        get_app_host_name_and_dev_key: get_app_host_name_and_dev_key,
        get_host_dev_key_and_bundle: get_host_dev_key_and_bundle,
        increase_sub1_unic_installs: increase_sub1_unic_installs,
        get_sub1_unic_installs: get_sub1_unic_installs,
        clear_sub1_unic_installs: clear_sub1_unic_installs,
        set_sub_id_tracker_url: set_sub_id_tracker_url,
        pause_sub_id_tracker_url: pause_sub_id_tracker_url,
        resume_sub_id_tracker_url: resume_sub_id_tracker_url,
        get_sub_id_tracker_url: get_sub_id_tracker_url,
        get_sub_id_tracker_url_cached: get_sub_id_tracker_url_cached,
        delete_sub_id_tracker_url: delete_sub_id_tracker_url
    };
};

const redisClient = require('./redis-client');

const separator = "%1$s"
const all_apps = 'all_apps'
const default_tracker_url_1 = 'default_tracker_url_1'
const default_tracker_url_2 = 'default_tracker_url_2'
const app_host_name = 'app_host_name'
const app_host_apps_dev_key = 'app_host_apps_dev_key'
const app_geo_split = "app_geo_split_"
const app_json_template = "app_json_template_"
const app_tracker_url_1 = "app_tracker_url_1_"
const app_tracker_url_2 = "app_tracker_url_2_"
const app_open_in_wv = "app_open_in_wv_"
const app_filter_vpn = "app_filter_vpn_"
const sub_id_installs_count = "sub_id_installs_count"

const host_name_ios_app_id = '_IOS_HOST_'

const sub_id_tracker_url = "sub_id_tracker_url"
const sub_id_tracker_url_cached = "sub_id_tracker_url_cached"

const URL_1_1 = '%URL1_PART1%'
const URL_1_2 = '%URL1_PART2%'
const URL_2_1 = '%URL2_PART1%'
const URL_2_2 = '%URL2_PART2%'

function set_sub_id_tracker_url(bundle_back, sub1, tracker_url) {
    redisClient.setAsync(sub_id_tracker_url_cached + sub1 + bundle_back, tracker_url)
}

function pause_sub_id_tracker_url(bundle_back, sub1) {
    redisClient.client.del(sub_id_tracker_url + sub1 + bundle_back)
}

async function resume_sub_id_tracker_url(bundle_back, sub1) {
    const tracker_url = await redisClient.getAsync(sub_id_tracker_url_cached + sub1 + bundle_back)
    redisClient.setAsync(sub_id_tracker_url + sub1 + bundle_back, tracker_url)
}

async function get_sub_id_tracker_url(bundle_back, sub1, callback) {
    const tracker_url = await redisClient.getAsync(sub_id_tracker_url + sub1 + bundle_back)
    callback(tracker_url)
}

async function get_sub_id_tracker_url_cached(bundle_back, sub1, callback) {
    const tracker_url = await redisClient.getAsync(sub_id_tracker_url_cached + sub1 + bundle_back)
    callback(tracker_url)
}

function delete_sub_id_tracker_url(bundle_back, sub1) {
    redisClient.client.del(sub_id_tracker_url + sub1 + bundle_back)
    redisClient.client.del(sub_id_tracker_url_cached + sub1 + bundle_back)
}

function increase_sub1_unic_installs(sub1) {
    get_sub1_unic_installs(sub1, async (installs) => {
        await redisClient.setAsync(sub_id_installs_count + sub1, (installs + 1).toString())
        console.log('sub id installs increase')
        console.log(installs + 1)
        console.log(sub1)
    })
}

async function get_sub1_unic_installs(sub1, callback) {
    const installs_str = await redisClient.getAsync(sub_id_installs_count + sub1)
    console.log('sub id installs get')
    console.log(installs_str)
    console.log(sub1)
    if (installs_str != null)
        callback(parseInt(installs_str))
    else
        callback(0)
}

function clear_sub1_unic_installs(sub1) {
    redisClient.client.del(sub_id_installs_count + sub1)
}

async function add_app(bundle_id, callback) {
    const rawData = await redisClient.getAsync(all_apps);
    if (rawData == null)
    {
        await redisClient.setAsync(all_apps, bundle_id);
    }
    else if (!rawData.includes(bundle_id))
    {
        await redisClient.setAsync(all_apps, rawData + separator + bundle_id);
    }
    callback('Success')
}

async function remove_app(bundle_id, callback) {
    get_apps_list(async (apps) => {
        if (apps == null)
        {
            console.log('No apps to delete')
            callback('No apps to delete')
        }
        else {
            let newApps = ""
            for (let i = 0; i < apps.length; i++) {
                let app = apps[i]
                if (app !== bundle_id)
                {
                    newApps += app
                    if ((i + 1) < apps.length)
                        newApps += separator
                }
            }
            console.log('apps before')
            console.log(apps.toString())
            console.log('apps after')
            console.log(newApps)
            if (newApps.length <= 0)
            {
                redisClient.client.del(all_apps)
                redisClient.client.del(app_host_name + bundle_id)
                redisClient.client.del(app_host_apps_dev_key + bundle_id)
                redisClient.client.del(app_geo_split + bundle_id)
                redisClient.client.del(app_json_template + bundle_id)
                redisClient.client.del(app_tracker_url_1 + bundle_id)
                redisClient.client.del(app_tracker_url_2 + bundle_id)
                redisClient.client.del(app_open_in_wv + bundle_id)
                console.log('deleting finished')
                callback('Success')
            }
            else {
                await redisClient.setAsync(all_apps, newApps);
                callback('Success')
            }
        }
    })
}

async function get_apps_list(callback) {
    const rawData = await redisClient.getAsync(all_apps);
    if (rawData != null)
        callback(rawData.split(separator))
    else
        callback(rawData)
}

async function add_app_geo_split(bundle_id, data, callback) {
    if (isJson(data)) {
        await redisClient.setAsync(app_geo_split + bundle_id, data);
        callback('Success')
    }
    else
        callback('Fail')
}

async function get_app_geo_split(bundle_id, callback) {
    const rawData = await redisClient.getAsync(app_geo_split + bundle_id);
    callback(rawData ? rawData : '')
}

async function get_app_geo_split_inapp(bundle_id, callback) {
    const rawData = await redisClient.getAsync(app_geo_split + bundle_id);
    callback(JSON.stringify(rawData ? (JSON.parse(rawData).inapp_ms !== undefined ? JSON.parse(rawData).inapp_ms : JSON.parse(rawData)) : {}))
}

async function get_app_geo_split_uac(bundle_id, callback) {
    const rawData = await redisClient.getAsync(app_geo_split + bundle_id);
    callback(JSON.stringify(rawData ? (JSON.parse(rawData).uac_ms !== undefined ? JSON.parse(rawData).uac_ms : JSON.parse(rawData)) : {}))
}

async function add_app_open_in_wv(bundle_id, data, callback) {
    await redisClient.setAsync(app_open_in_wv + bundle_id, data);
    callback('Success')
}

async function get_app_open_in_wv(bundle_id, callback) {
    const rawData = await redisClient.getAsync(app_open_in_wv + bundle_id);
    callback(rawData ? rawData : 'true')
}

async function add_app_filter_vpn(bundle_id, data, callback) {
    await redisClient.setAsync(app_filter_vpn + bundle_id, data);
    callback('Success')
}

async function get_app_filter_vpn(bundle_id, callback) {
    const rawData = await redisClient.getAsync(app_filter_vpn + bundle_id);
    callback(rawData ? rawData : 'true')
}

async function add_app_tracker_url(bundle_id, data1, data2, callback) {
    await redisClient.setAsync(app_tracker_url_1 + bundle_id, data1);
    await redisClient.setAsync(app_tracker_url_2 + bundle_id, data2);
    callback('Success')
}

async function get_app_tracker_url(bundle_id, callback) {
    const rawData1 = await redisClient.getAsync(app_tracker_url_1 + bundle_id);
    const rawData2 = await redisClient.getAsync(app_tracker_url_2 + bundle_id);
    if (rawData1 != null)
        callback(rawData1, rawData2 ? rawData2 : rawData1)
    else
        await get_app_default_tracker_url(callback)
}

async function add_app_default_tracker_url(data1, data2, callback) {
    await redisClient.setAsync(default_tracker_url_1, data1);
    await redisClient.setAsync(default_tracker_url_2, data2);
    callback('Success')
}

async function get_app_default_tracker_url(callback) {
    const rawData1 = await redisClient.getAsync(default_tracker_url_1);
    const rawData2 = await redisClient.getAsync(default_tracker_url_2);
    const defaultUrl = 'http://213.139.210.76/fX7CDf?t1={random_offer}&t2={device_id}'
    callback(rawData1 ? rawData1 : defaultUrl, rawData2 ? rawData2 : defaultUrl)
}

async function add_app_host_name_and_dev_key(bundle_id, app_id, host_name, dev_key, callback) {
    await redisClient.setAsync(bundle_id + host_name_ios_app_id, app_id ? app_id : '');
    await redisClient.setAsync(app_host_name + bundle_id, host_name);
    await redisClient.setAsync(app_host_apps_dev_key + bundle_id, dev_key);
    await redisClient.setAsync(host_name, bundle_id);
    callback('Success')
}

async function get_app_host_name_and_dev_key(bundle_id, callback) {
    const host_name = await redisClient.getAsync(app_host_name + bundle_id);
    const devKey = await redisClient.getAsync(app_host_apps_dev_key + bundle_id);
    const app_id = await redisClient.getAsync(bundle_id + host_name_ios_app_id)
    const defaultHostName = '135.181.177.241'
    const defaultDevKey = 'dkef3nDPGtQixBzU8DxbGT'
    callback(host_name ? host_name : defaultHostName, devKey ? devKey : defaultDevKey, app_id)
}

async function get_host_dev_key_and_bundle(host_name, callback) {
    const bundle_id = await redisClient.getAsync(host_name);
    const app_id_ios = await redisClient.getAsync(bundle_id + host_name_ios_app_id);
    const defaultBundleId = bundle_id ? bundle_id : 'com.addeew.affoeo'
    const devKey = await redisClient.getAsync(app_host_apps_dev_key + defaultBundleId);
    const defaultDevKey = devKey ? devKey : 'dkef3nDPGtQixBzU8DxbGT'
    callback(defaultDevKey, app_id_ios ? app_id_ios : defaultBundleId)
}

async function add_app_json_template(bundle_id, data, callback) {
    if (!isJson(data) || !data.includes(URL_1_1)
        || !data.includes(URL_1_2)
        || !data.includes(URL_2_1)
        || !data.includes(URL_2_2)) {
        callback('Fail')
        return
    }
    let u11, u12, u21, u22
    const data_json = JSON.parse(data)
    for (let key in data_json) {
        if (data_json[key] === URL_1_1)
            u11 = key
        if (data_json[key] === URL_1_2)
            u12 = key
        if (data_json[key] === URL_2_1)
            u21 = key
        if (data_json[key] === URL_2_2)
            u22 = key
    }
    const data_processed = {
        data_template: data,
        url_1_1: u11,
        url_1_2: u12,
        url_2_1: u21,
        url_2_2: u22
    }
    await redisClient.setAsync(app_json_template + bundle_id, JSON.stringify(data_processed));
    callback('Success')
}

async function get_app_json_template(bundle_id, callback) {
    const rawData = await redisClient.getAsync(app_json_template + bundle_id);
    callback(rawData ? rawData : '{}')
}

function isJson(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}
